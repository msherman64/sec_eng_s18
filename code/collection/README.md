# Data Collection

## Network
![mal-net-1](./mal-net-1.png)

## Captures
- **Normal Traffic:** 5 user hosts sending periodic SSH commands and
HTTP GET requests to the server.
- **DDoS:** Slowloris DDOES attack against server.
- **Brute Force:** Hydra SSH brute force login attack against server.
- **NMap:** Continuous port scan of server.

| Name                 | Description                                                   |
|----------------------|---------------------------------------------------------------|
| brute_5m.pcap        | 5 minute capture of SSH brute-force attack.                   |
| ddos_5m.pcap         | 5 minute capture of Slowloris DDoS attack.                    |
| nmap_5m.pcap         | 5 minute capture of nmap scan.                                |
| normal_5m.pcap       | 5 minute capture of SSH and HTTP user traffic.                |
| normal_brute_5m.pcap | 5 minute capture of normal traffic with brute force attack.   |
| normal_ddos_5m.pcap  | 5 minute capture of normal traffic and Slowloris DDoS attack. |
| normal_nmap_5m.pcap  | 5 minute capture of normal traffic with nmap scan.            |

### Brute-Force Attack
The brute-force attack performed was a simple SSH attack using hydra.

`hydra <target ip> ssh -L <user list> -P <password list>`

### Nmap
Nmap scans were simply repeated nmap scans against the host. Since we're using
the same services it may be more interesting to restrict scans to the same set
of ports repeatedly.

`(while true; do nmap -Pn <target ip>; done)`

### DDoS
Spawns 150 sockets to target IP generating HTTP requests conducting a DDoS attack.

`slowloris <target ip>`

### Filetypes
.pcap files are the raw packet captures.
.sample files have been processed with the kdd99 collector
.labeled files are .samples, with a label column added for validation

## Data Collected
| Feature  | Type      | Description    | 
| -------- | --------- | -------------- | 
| duration | Continuous | Length in seconds of connection | 
| protocol_type | Symbolic | Type of transport protocol (tcp/udp) | 
| service | Symbolic | Network service (http/ssh) | 
| flag | Symbolic | Status flag of connection (alive?) | 
| src_bytes | Continuous | Bytes sent from source to destination | 
| dst_bytes | Continuous | Bytes sent from destination to source | 
| land | Symbolic | 1 if connection from/to the same host/port; 0 otherwise | 
| wrong_fragment | Continuous | Number of wrong fragments | 
| urgent | Continuous | Number of urgent packets (TCP flag) | 
| count | Continuous | Number of connections to the same host as the current connection in the past two seconds | 
| srv_count | Continuous | Number of connections to the same service as the current connection in the past two seconds | 
| serror_rate | Continuous | Percent of connections that have "SYN" errors (TCP seq number ACK sync) | 
| srv_serror_rate | Continuous | Percent of connections that have "SYN" errors (TCP seq number ACK sync) | 
| rerror_rate | Continuous | Percent of connections that have "REJ" errors (connection attempt rejected) | 
| srv_rerror_rate | Continuous | Percent of connections that have "REJ" errors  (connection attempt rejected) | 
| same_srv_rate | Continuous | Percent of connections to the same service | 
| diff_srv_rate | Continuous | Percent of connections to different services | 
| srv_diff_host_rate | Continuous | Percent of connections to different hosts | 
| dst_host_count | Continuous | Count of connections having the same destination host | 
| dst_host_srv_count | Continuous | Count of connections having the same destination host and using the same service | 
| dst_host_same_srv_rate | Continuous | Percent of connections having the same destination host and using the same service | 
| dst_host_diff_srv_rate | Continuous | Percent of different services on the current host | 
| dst_host_same_src_port_rate | Continuous | Percent of connections to the current host having the same source port | 
| dst_host_srv_diff_host_rate | Continuous | Percent of connections to the same service coming from different hosts | 
| dst_host_serror_rate | Continuous | Percent connections to the current host and specified service that have an "RST" error (connection reset) | 
| dst_host_srv_serror_rate | Continuous | Percent connections to the current host and specified service that have an "S0" error (connection attempt seen but no reply) | 
| dst_host_rerror_rate | Continuous | Percent connections to the current host that have an "RST" error (connection reset) | 
| dst_host_srv_rerror_rate | Continuous | Percent connections to the current host that have an "RST" error (connection reset) | 
