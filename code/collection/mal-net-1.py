"""Custom topology example

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo
from mininet.node import Node


class MalNetTopo(Topo):
    "Topology to capture malicious traffic mixed with regular."

    def __init__( self ):
        # Initialize topology
        Topo.__init__(self)

        # User net
        dr = 'via 172.16.0.1'
        u_dip = '172.16.0.1/24'

        switch = self.addSwitch('sw1')

        # Create a server node
        server = self.addHost('s1', ip='172.16.0.100', defaultRoute=dr)
        self.addLink(server, switch)

        # Initialize all user hosts to user switch
        for x in xrange(5):
            node_name = 'uh{}'.format(x+1)
            node_ip = '172.16.0.{}/24'.format(x+101)
            h = self.addHost(node_name, ip=node_ip, defaultRoute=dr)
            self.addLink(h, switch)

        # Create an attacker node
        attacker = self.addHost('ah1', ip='172.16.0.66', defaultRoute=dr)
        self.addLink(attacker, switch)


topos = {'mal-net': (lambda: MalNetTopo())}
