import os
import pexpect
import random
import requests
import sys
import threading
import time


lock = threading.Lock()


# Time between requests
SLEEP_TIME = 1


# Read environment variables for ssh session
SERVER_IP = '172.16.0.100'
SSH_USER = 'mininet'
SSH_PASSWORD = 'mininet'


# Number of ssh sessions to spawn for traffic
SESSION_CNT = 3


TOKEN_PROMPT = '\?'
PASSWORD_PROMPT = 'password:'
TERMINAL_PROMPT = '\$'


# Commands to run as test traffic
cmds = ('ls', 'ps', 'cd .', 'echo hello')
http_files = ('test1.md', 'test2.txt', 'test3.cpp', 'test4.txt', 'invalid_file')


class SSHSession():

    def __init__(self):
        ssh_cmd = 'ssh {}@{}'.format(SSH_USER, SERVER_IP)
        ssh_session = pexpect.spawn(ssh_cmd)
        while True:
            i = ssh_session.expect([PASSWORD_PROMPT, TOKEN_PROMPT])
            if i == 0:
                ssh_session.sendline(SSH_PASSWORD)
                break
            elif i == 1:
                ssh_session.sendline('yes')
            else:
                 print 'unexpected error'
                 sys.exit(-1)
        ssh_session.expect(TERMINAL_PROMPT)
        print 'Login success!'
        self.session = ssh_session

    def sim_user(self):
        while True:
            idx = random.randint(0, len(cmds) - 1)
            cmd = cmds[idx]
            self.session.sendline(cmd)
            self.session.expect(TERMINAL_PROMPT)
            with lock:
                print 'SSH: command "{}" executed'.format(cmd)
            time.sleep(SLEEP_TIME)


def http_get_request():
    while True:
        idx = random.randint(0, len(http_files) - 1)
        fname = http_files[idx]
        r = requests.get('http://{}/{}'.format(SERVER_IP, fname))
        with lock:
            print 'HTTP: get {} status code {}'.format(fname, r)
        time.sleep(SLEEP_TIME)


if __name__ == '__main__':
    ssh = SSHSession()
    t1 = threading.Thread(target=http_get_request)
    t1.setDaemon(True)
    t2 = threading.Thread(target=ssh.sim_user)
    t2.setDaemon(True)
    t1.start()
    t2.start()
    while True: pass
