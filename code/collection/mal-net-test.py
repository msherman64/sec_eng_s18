"""Custom topology example

Two directly connected switches plus a host for each switch:

   host --- switch --- switch --- host

Adding the 'topos' dict with a key/value pair to generate our newly defined
topology enables one to pass in '--topo=mytopo' from the command line.
"""

from mininet.topo import Topo
from mininet.node import Node


class LinuxRouter(Node):

    def config(self, **params):
        super(LinuxRouter, self).config(**params)
        self.cmd('sysctl net.ipv4.ip_forward=1')

    def terminate(self):
        self.cmd('sysctl net.ipv4.ip_forward=0')
        super(LinuxRouter, self).terminate()


class MalNetTopo(Topo):
    "Topology to capture malicious traffic mixed with regular."

    def __init__( self ):
        # Initialize topology
        Topo.__init__(self)

        # Server net
        s_dip = '192.168.1.1/24'
        s_dgw = 'via 192.168.1.1'

        # User net
        u_dip = '172.16.0.1/24'
        u_dgw = 'via 172.16.0.1'

        # Router to connect switch to server
        router = self.addNode('r', cls=LinuxRouter, ip=s_dip)

        u_switch = self.addSwitch('sw1')
        s_switch = self.addSwitch('sw2')

        # User Net <-> Router <-> Server Net
        self.addLink(u_switch, router, intfName2='r-eth1', params2={'ip': u_dip})
        self.addLink(s_switch, router, intfName2='r-eth2', params2={'ip': s_dip})

        # Create a server node
        server = self.addHost('s1', ip='192.168.1.100', defaultRoute=s_dgw)
        self.addLink(server, s_switch, params2={'ip': s_dip})

        # Initialize all user hosts to user switch
        for x in xrange(5):
            node_name = 'uh{}'.format(x+1)
            node_ip = '172.16.0.{}/24'.format(x+101)
            h = self.addHost(node_name, ip=node_ip, defaultRoute=u_dgw)
            self.addLink(h, u_switch)

        # Create an attacker node
        attacker = self.addHost('ah1', ip='172.16.0.66', defaultRoute=u_dgw)
        self.addLink(attacker, u_switch)


topos = {'mal-net': (lambda: MalNetTopo())}
