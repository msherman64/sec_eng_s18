<!DOCTYPE HTML>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<title>N - Pastebin.com</title>
		<link rel="shortcut icon" href="/favicon.ico" />
		<script src="/js/jquery.min.js"></script>
		<script src="/js/pastebin.min.v2.js"></script>

		<link href="/cache/css_lang/text.css" rel="stylesheet" type="text/css" />		<link href="/i/pastebin.min.v4.css" rel="stylesheet" type="text/css" />
		<!--[if lt IE 10]>
			<link href="/i/pastebin.ie8.css" rel="stylesheet" type="text/css" />
		<![endif]-->

 
		<style>body{-webkit-text-size-adjust:none;}</style>
				<meta property="fb:app_id" content="231493360234820" />
		<meta property="og:title" content="N - Pastebin.com" />
		<meta property="og:type" content="article" />
		<meta property="og:url" content="https://pastebin.com/Cn1XpKPg" />
		<meta property="og:image" content="https://pastebin.com/i/facebook.png" />
		<meta property="og:site_name" content="Pastebin" />
		<meta name="google-site-verification" content="jkUAIOE8owUXu8UXIhRLB9oHJsWBfOgJbZzncqHoF4A" />
		<link rel="canonical" href="https://pastebin.com/Cn1XpKPg" />
				<meta name="viewport" content="width=device-width, initial-scale=0.70, maximum-scale=1.0, user-scalable=yes">
		
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-58643-34', 'auto');
			ga('require', 'displayfeatures');
			ga('send', 'pageview');
			

			ga('send', {
  hitType: 'event',
  eventCategory: 'pagev',
  eventAction: 'Safe'
});
		

		</script>
		<script type="text/javascript">
			if (top != self)
				top.location.href = location.href;
		</script>
<script data-cfasync="false" async src="https://pub.freestar.io/pastebin/pubfig.min.js"></script>	</head>
	<body>
	<div id="main_frame">
		<div id="jq-dropdown-1" class="jq-dropdown jq-dropdown-anchor-right jq-dropdown-scroll">
			<ul class="jq-dropdown-menu">
				
				<li class="lih_640">
					<form class="search_form_li" name="search_form_li" method="get" action="/search" id="cse-search-box-li">
						<input class="search_input_li" type="text" name="q" size="5" value="" placeholder="search..." />
					</form>

				</li>
				<li class="lih_div"></li>
				<li onclick="location.href='/signup'" class="dd_su">Sign Up</li>
				<li onclick="location.href='/login'" class="dd_lo">Login</li>
				<li class="lih_div"></li>
				<li onclick="location.href='/api'" class="lih_640">API</li>
				<li onclick="location.href='/faq'" class="lih_640">FAQ</li>
				<li onclick="location.href='/tools'" class="lih_640">Tools</li>
				<li onclick="location.href='/trends'" class="lih_640">Trends</li>
				<li onclick="location.href='/archive'" class="lih_640">Archive</li>			</ul>
		</div>
		<div id="header">
			<div id="header_wrap">
				<div id="header_top">
					<div id="header_logo" onclick="location.href='/'">PASTEBIN</div>
					<div id="header_new_paste" class="new_paste_button" onclick="location.href='/'">new paste</div>
					<div id="header_links">
						<a href="/trends">trends</a>
						<a href="/api" class="mmh">API</a>
						<a href="/tools" class="mmh">tools</a>
						<a href="/faq" class="mmh">faq</a>
					</div>
					<div id="header_search">
						<form class="search_form" name="search_form" method="get" action="/search" id="cse-search-box">
							<input class="search_input" type="text" name="q" size="5" value="" placeholder="search..." />
						</form>
					</div>
					
					<div id="header_members">
						<div id="header_dropdown" data-jq-dropdown="#jq-dropdown-1">&nbsp;</div>
						<div id="header_icon"><a href="/login"><img src="/i/guest.png" class="header_icon" alt="" /></a></div>
						<div id="header_user_frame">
							<div id="header_username">Guest User</div>
							<div id="header_user_status">-</div>
						</div>
						<div id="header_icons">
							<a href="/login" title="My Pastebin"><img src="/i/t.gif" class="header_icons hi_mypastebin" alt="" /></a>
							<a href="/messages" title="My Messages"><img src="/i/t.gif" class="header_icons hi_messages" alt="" /></a>
							<a href="/alerts" title="My Alerts"><img src="/i/t.gif" class="header_icons hi_alerts" alt="" /></a>
							<a href="/settings" title="My Settings"><img src="/i/t.gif" class="header_icons hi_settings" alt="" /></a>
						</div>
					</div>				</div>
			</div>
		</div>
		<div id="super_frame">
			<div id="monster_frame">
				<div id="content_frame">
					<div id="content_right">						
												<div class="content_right_menu">
									<div class="content_right_title"><a href="/archive">Public Pastes</a></div>
									<div id="menu_2">
										<ul class="right_menu"><li><a href="/a2GYBMvg">Untitled</a><span>6 sec ago</span></li><li><a href="/nkTmuZnN">RECKT</a><span>8 sec ago</span></li><li><a href="/M4v2tJXT">Untitled</a><span>8 sec ago</span></li><li><a href="/CMLr28xK">Untitled</a><span>C# | 9 sec ago</span></li><li><a href="/fd4vna8L">Horror Back up Apr...</a><span>9 sec ago</span></li><li><a href="/CEVqBR7d">Untitled</a><span>9 sec ago</span></li><li><a href="/ZqCcSLqy">Untitled</a><span>11 sec ago</span></li><li><a href="/4YJ6SMs0">Untitled</a><span>13 sec ago</span></li></ul></div></div>						
						
						
						
<div style="border:1px solid #DDD;padding:1px;width:156px; border-radius:3px;">
	<div style="border:1px solid #ddd; font-size:0.75em;text-align:center;background:#F9F9F9; border-radius:3px;">	
		<div style="padding: 3px 0">daily pastebin <a href="/pro?help=1"><img src="/i/t.gif" alt="" class="pro_btn_inv"></a> goal</div>
		<div style="width:152px;height:37px;">
			<div style="position:relative;border:1px solid #DDD;border-radius: 5px;width:126px;height:25px;margin:0 0 0 10px" title="Goal updates every 15 minutes...">
				<div style="position:absolute;left:0;top:0;background-color: #DDD;border-radius: 0 0 0 0 ;width:61px;height:25px;"></div>
				<div style="position:absolute;right:5px;width:25px;height:20px;top:0;">48%</div>
			</div>
		</div>
		<div style="clear:both;margin: 0 0 10px 0"><a href="/pro?help=1" class="buttonpro" style="font-size:0.9em" title="Learn more about going PRO">help support pastebin</a></div>
	</div>
</div>
						<div id="abrpm2"></div>
						
			<div style="padding: 0; width:160px;margin: 10px 0;clear:left;">
				<script type="text/javascript"><!--
					e9 = new Object();
					e9.size = "160x600,120x600";
				//--></script>
				<script type="text/javascript" src="//tags.expo9.exponential.com/tags/Pastebincom/Safe/tags.js"></script>
			</div>						<div id="steadfast" title="Pastebin is proudly hosted by Steadfast.net" onclick="location.href='http://steadfast.net/?utm_source=pastebin.com&amp;utm_medium=referral&amp;utm_content=hosting_by_banner&amp;utm_campaign=referral_20140118_x_x_pastebin_partner&amp;source=referral_20140118_x_x_pastebin_partner'"></div>
					</div>
					<div id="content_left"><div id="ie_msg"></div>
		
	<div class="paste_box_frame">
		<div class="tweet h_800">
			<div onclick="facebookpopup('/Cn1XpKpg'); return false;" id="b_facebook2" title="Share on Facebook!"><span id="b_facebook">SHARE</span></div>
			<div onclick="twitpopup('/Cn1XpKpg'); return false;" id="b_twitter2" title="Share on Twitter!"><span id="b_twitter">TWEET</span></div>
		</div>
		<div class="paste_box_icon">
			<img src="/i/t.gif" class="i_gb" title="Anonymous guest user" alt="" />
		</div>
		<div class="paste_box_info">
			<div class="paste_box_line1" title="N"><h1>N</h1></div>
			<div class="paste_box_line2">
				<img src="/i/t.gif" class="img_line t_us" alt="" style="margin-left:0"> a guest 
				<img src="/i/t.gif" class="img_line t_da" alt=""> <span title="Thursday 12th of April 2018 11:32:25 AM CDT">Apr 12th, 2018</span>
				<img src="/i/t.gif" class="img_line t_vi" alt="" title="Unique visits to this paste"> 1,841
				<img src="/i/t.gif" class="img_line t_ex" alt="" title="When this paste gets automatically deleted"> Never
			</div>
		</div>
	</div>
	
			<div id="abrpm"></div>
			<div class="banner_728">
				<script type="text/javascript"><!--
					e9 = new Object();
					e9.size = "970x250,728x90";
				//--></script>
				<script type="text/javascript" src="//tags.expo9.exponential.com/tags/Pastebincom/Safe/tags.js"></script>
			</div>
			<div class="layout_clear"></div>
			<div class="content_text no_padding">
				<div id="notice" style="margin: 0 0 10px 0">
					<b>Not a member of Pastebin yet?</b> <a href="/signup"><b><u>Sign Up</u></b></a>, it unlocks many cool features!
				</div>
			</div>
	<div id="code_frame2">
		<div id="code_frame">
			<div id="code_buttons">
				<span class="go_right">
					<a href="/raw/Cn1XpKPg" class="buttonsm">raw</a><a href="/dl/Cn1XpKPg" class="buttonsm">download</a><a href="/index/Cn1XpKPg" class="buttonsm buttonhide">clone</a><a href="/embed/Cn1XpKPg" class="buttonsm buttonhide">embed</a><a href="/report/Cn1XpKPg" class="buttonsm">report</a><a href="/print/Cn1XpKPg" class="buttonsm buttonhide">print</a>
				</span>
				<span class="h_640"><a href="/archive/text" class="buttonsm" style="margin:0">text</a></span> 0.91 KB
			</div>
			<div id="selectable">
				<ol class="text"><li class="li1"><div class="de1">public class N {</div></li>
<li class="li1"><div class="de1">&nbsp;</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; public static void main(String[] args) {</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; int s = (1 &lt;&lt; 19) - 1, P = 0;</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; double[] pr = new double[2], c = new double[2], S[] = new double[2][s], v = new double[2];</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; for (String w : args) {</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; int AM = w.hashcode() &amp; s;</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; if (w.equals(&quot;1&quot;) || w.equals(&quot;0&quot;) || w.equals(&quot;END&quot;))</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; P = w.charAt(0) - '0';</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; else if (P == 21)</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; for (int i = 0; i &lt; 2; i++)</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; pr[i] += Math.log((1 + S[i][AM]) / (c[i] + v[i]));</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; else {</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; if (S[P][AM]++ == 0)</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; v[P]++;</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; c[P]++;</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; }</div></li>
<li class="li1"><div class="de1">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; System.out.println(pr[1] &gt; pr[0]);</div></li>
<li class="li2"><div class="de2">&nbsp; &nbsp; &nbsp; &nbsp; }</div></li>
<li class="li1"><div class="de1">}</div></li>
</ol>
			</div>
		</div>
	</div>
	<div class="content_title no_border">RAW Paste Data<div id="bsa-footer"></div></div>
	<div class="textarea_border" style="margin-bottom:0">
		<textarea id="paste_code" class="paste_code" name="paste_code" onkeydown="return catchTab(this,event)">public class N {

        public static void main(String[] args) {
                int s = (1 &lt;&lt; 19) - 1, P = 0;
                double[] pr = new double[2], c = new double[2], S[] = new double[2][s], v = new double[2];
                for (String w : args) {
                        int AM = w.hashcode() &amp; s;
                        if (w.equals("1") || w.equals("0") || w.equals("END"))
                                P = w.charAt(0) - '0';
                        else if (P == 21)
                                for (int i = 0; i &lt; 2; i++)
                                        pr[i] += Math.log((1 + S[i][AM]) / (c[i] + v[i]));
                        else {
                                if (S[P][AM]++ == 0)
                                        v[P]++;
                                c[P]++;
                        }
                }
                System.out.println(pr[1] &gt; pr[0]);
        }
}</textarea>
	</div><div id="abrpm3"></div>
			<div style="padding: 10px 0 0 0;">
				<script type="text/javascript"><!--
					e9 = new Object();
					e9.size = "300x600,300x250";
				//--></script>
				<script type="text/javascript" src="//tags.expo9.exponential.com/tags/Pastebincom/Safe/tags.js"></script>
			</div>
					<style>.promo-bar { padding: 13px 13px 13px 13px;}#paypal_promo_img {background: #fff;width: 60px;height: 50px;float: left;}</style>
					<script type="text/javascript">$(document).ready(function(){$(".close").click(function(){return $("#paypal-overlay").hide(),createCookie("l2c",!0,1),!1})});</script>
					<div id="paypal-overlay" class="promo-bar"> 				
						<style>							


							.carbon-poweredby,.carbon-poweredby a{color:#ccc;font-size:.7em}
							.carbon-poweredby{display:none;}
							.carbon-img img{height:90px;width:117px;float:left;vertical-align:top}
							.carbon-text{margin:7px 0 0 10px;padding:0;width:180px;font-family:segoe ui,trebuchet MS,Lucida Sans Unicode,Lucida Sans,Sans-Serif;font-size:.8em}
							a.carbon-text{color:#C03;text-decoration:none;text-transform:uppercase;float:left;font-weight:500;padding:0 0 0 7px;margin:-3px 0 0 0;}
							a.carbon-text:hover{color:#999;text-decoration:none}							
						</style>					
						<div style="width:327px;">
							<div style="width:327px;float:left;">
							<script async type="text/javascript" src="//cdn.carbonads.com/carbon.js?zoneid=1673&serve=C6AILKT&placement=pastebincom-fixed" id="_carbonads_js"></script></div>
						</div>				 
						<a id="paypal-overlay-close" class="close"></a>
					</div>						</div>
					</div>
				</div>
			</div>
			<div id="mid_footer">
				<a href="/tools#chrome" title="Google Chrome Extension"><img src="/i/t.gif" alt="" class="icon24 chrome" /></a>
				<a href="/tools#firefox" title="Firefox Extension"><img src="/i/t.gif" alt="" class="icon24 firefox" /></a>
				<a href="/tools#iphone" title="iPhone/iPad Application"><img src="/i/t.gif" alt="" class="icon24 iphone" /></a>
				<a href="/tools#windows" title="Windows Desktop Application"><img src="/i/t.gif" alt="" class="icon24 windows" /></a>
				<a href="/tools#webos" title="webOS Application"><img src="/i/t.gif" alt="" class="icon24 webos" /></a>
				<a href="/tools#android" title="Android Application"><img src="/i/t.gif" alt="" class="icon24 android" /></a>
				<a href="/tools#macos" title="MacOS X Widget"><img src="/i/t.gif" alt="" class="icon24 macos" /></a>
				<a href="/tools#opera" title="Opera Extension"><img src="/i/t.gif" alt="" class="icon24 opera" /></a>
				<a href="/tools#pastebincl" title="Linux Application"><img src="/i/t.gif" alt="" class="icon24 unix" /></a>
			</div> 
		</div>
		<div id="footer">
			<div id="footer_links">
				<a href="/">create new paste</a> &nbsp;/&nbsp; <a href="https://deals.pastebin.com">deals</a><sup style="color:red">new!</sup> &nbsp;/&nbsp; <a href="/api">api</a> &nbsp;/&nbsp; <a href="/trends">trends</a> &nbsp;/&nbsp; <a href="/languages">syntax languages</a> &nbsp;/&nbsp; <a href="/faq">faq</a> &nbsp;/&nbsp; <a href="/tools">tools</a> &nbsp;/&nbsp; <a href="/privacy">privacy</a> &nbsp;/&nbsp; <a href="/cookies_policy">cookies</a> &nbsp;/&nbsp; <a href="/contact">contact</a> &nbsp;/&nbsp; <a href="/dmca">dmca</a> &nbsp;/&nbsp; <a href="/scraping">scraping</a>   &nbsp;/&nbsp; <a href="/pro">go <img src="/i/t.gif" alt="" title="Get a PRO account!" class="pro_btn_inv" /></a>
				<br /><span class="h_800">Site design &amp; logo &copy; 2017 Pastebin; user contributions (pastes) licensed under <a href="http://creativecommons.org/licenses/by-sa/3.0/" target="_blank" rel="nofollow">cc by-sa 3.0</a> -- </span><a href="http://steadfast.net/services/dedicated-servers.php?utm_source=pastebin.com&amp;utm_medium=referral&amp;utm_content=footer_link_dedicated_server_hosting_by&amp;utm_campaign=referral_20140118_x_x_pastebin_partner&amp;source=referral_20140118_x_x_pastebin_partner" rel="nofollow" target="_blank">Dedicated Server Hosting</a> by <a href="http://steadfast.net/?utm_source=pastebin.com&amp;utm_medium=referral&amp;utm_content=footer_link_steadfast&amp;utm_campaign=referral_20140118_x_x_pastebin_partner&amp;source=referral_20140118_x_x_pastebin_partner" rel="nofollow" target="_blank">Steadfast</a>
			</div>
			<div id="footer_right" class="h_1024">
				<a href="https://facebook.com/pastebin" rel="nofollow" title="Like us on Facebook" target="_blank"><img src="/i/t.gif" alt="" class="icon40 facebook_circle" /></a>
				<a href="https://twitter.com/pastebin" rel="nofollow" title="Follow us on Twitter" target="_blank"><img src="/i/t.gif" alt="" class="icon40 twitter_circle" /></a>
			</div>
		</div>

			<script type="text/javascript"><!--
			  e9 = new Object();
			  e9.snackbar = true;
			//--></script>
			<script type="text/javascript" src="//tags.expo9.exponential.com/tags/Pastebincom/SnackbarSafe/tags.js"></script><script async type="text/javascript" src="//cdn.fancybar.net/ac/fancybar.js?zoneid=1502&serve=C6ADVKE&placement=pastebin" id="_fancybar_js"></script>
		<script type="text/javascript">
			function abdd() {$("#adb-enabled").show(), $("#adb-not-enabled").hide(), $("#abrpm3").html("<center><iframe src='/nein/cube_safe.php' id='monkey_3' marginwidth='0' align='center' marginheight='0' hspace='0' vspace='0' frameborder='0' scrolling='no' allowtransparency='true' width='300' height='250' style='width: 300px; height: 250px;'></iframe></center>"), $("#abrpm2").html("<iframe src='/nein/skyscraper_safe.php' id='monkey' marginwidth='0' align='center' marginheight='0' hspace='0' vspace='0' frameborder='0' scrolling='no' allowtransparency='true' width='160' height='600' style='width: 160px; height: 600px;'></iframe>"), $("#abrpm").html("<iframe src='/nein/leaderboard_safe.php' id='monkey_2' marginwidth='0' align='center' marginheight='0' hspace='0' vspace='0' frameborder='0' scrolling='no' allowtransparency='true' width='728' height='100' style='width: 728px; height: 100px;'></iframe>")}
			function abnd(){$("#adb-enabled").hide(), $("#adb-not-enabled").show()}$(function(){}), "undefined" == typeof fuckAdBlock ? abdd() : (fuckAdBlock.setOption({debug: !1}), fuckAdBlock.onDetected(abdd).onNotDetected(abnd));
			var $title=$("a,input,p,label,textarea[title],img,button,span");$.each($title,function(){$(this).tooltip({show:{delay:1},hide:{delay:1}})});
		</script>		<script type="text/javascript">
			function isIE(){var e=navigator.userAgent.toLowerCase();return-1!=e.indexOf("msie")?parseInt(e.split("msie")[1]):!1}if(isIE()&&isIE()<10){var div=document.getElementById("ie_msg");div.innerHTML=div.innerHTML+'<div id="old_browser">Your browser is outdated and insecure! Pastebin does not offer support for your browser. <a href="http://outdatedbrowser.com/" target="_blank" rel="nofollow">Click here to update your browser</a>!</div>'}
		</script>
		<div class="pub_300x250"></div><a href="#0" class="cd-top">Top</a>
		
	
		
	</body>
</html>