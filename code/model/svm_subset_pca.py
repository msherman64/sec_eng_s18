#!/usr/bin/env python3

import pandas as pd

#load list of field names, stripped attack types
#includes whether it's a continuous or symbolic value
feature_list = pd.read_csv('../../datasets/pandas/kddcup.names',delimiter=':|\n',header=None,engine='python')
names = feature_list[0].tolist()
#load dataset

#sklearn source, some processing
from sklearn.datasets import fetch_kddcup99
#available datasets none=entire, SA, SF, http, smtp
#percent10=true dl 10% for testing, SA is mostly 'normal' values
dataset_10p = fetch_kddcup99(subset='SA',data_home='../../datasets/')

#load data into dataframe, assign headers
dataframe = pd.DataFrame(dataset_10p.data, columns=names)

#remove fields from both dataframe and names
extractor_names = pd.read_csv('extractor_output_header.txt').columns.tolist()
#if a field is not in extractor names, remove it from dataframe and names
#excluding optional vals, as they're not in the dataset
dataframe = dataframe[extractor_names[:-6]]
feature_list = feature_list[feature_list[0].isin(extractor_names)]
#print(feature_list)


#get which cols are symbolic or continuous from names file
sym_cols = feature_list.loc[feature_list[1].str.strip() == 'symbolic.'][0]
cont_cols = feature_list.loc[feature_list[1].str.strip() == 'continuous.'][0]

from sklearn.preprocessing import StandardScaler,MultiLabelBinarizer,LabelBinarizer,LabelEncoder
#do scaling normalize columns to 0-1. This prevents unfair weighting
#fit sets scaler params #transform applies #fit_transforms does both. Will need to modify once have input data
scaled_features = dataframe.copy()
cont_features = scaled_features[cont_cols]
scaler = StandardScaler().fit(cont_features.values)
features = scaler.transform(cont_features.values)
scaled_features[cont_cols] = features

#convert symbolic features via onehot encoder prior to normalizing. this ensures no effect of ordering.
#better way combines factorize and onehot encoding
#fastml.com/how-to-use-pd-dot-get-dummies-with-the-test-set/

#do binary encoding of labels
scaled_onehot = pd.get_dummies(scaled_features, columns = sym_cols)

#process targets
target_df = pd.Series(dataset_10p.target, index=dataframe.index)

#Go from Multi-Class to binary classification
#target_df.replace(to_replace="\^(?!.*normal\.).*$",value='attack', regex=True,inplace=True)
target_df = target_df.replace(
        [b'back.', b'buffer_overflow.', b'ftp_write.', b'guess_passwd.', b'imap.', b'ipsweep.', 
            b'land.', b'loadmodule.', b'multihop.', b'neptune.', b'nmap.', b'perl.', b'phf.', 
            b'pod.', b'portsweep.', b'rootkit.', b'satan.', b'smurf.', b'spy.', b'teardrop.', b'warezclient.', b'warezmaster.'
            ], b'attack')

#lets run PCA to identify important fields
#guide from https://towardsdatascience.com/pca-using-python-scikit-learn-e653f8989e60
from sklearn.decomposition import PCA
pca_all = PCA(0.95)
tmp = pca_all.fit_transform(scaled_onehot)
print(pca_all.explained_variance_ratio_)
#we want to project to 2d for visualization
pca = PCA(n_components=1)
prinComp = pca.fit_transform(scaled_onehot)
print(pca.explained_variance_ratio_)
prin_df = pd.DataFrame(data=prinComp, columns = ['component 1'])
#prin_df = pd.DataFrame(data=prinComp, columns = ['component 1', 'component 2'])
#end_df = pd.concat([prin_df, target_df], axis = 1)
#
#
#import matplotlib.pyplot as plt
##visualize
#fig = plt.figure(figsize = (8,8))
#ax = fig.add_subplot(1,1,1)
#ax.set_xlabel('PC1', fontsize = 15)
#ax.set_ylabel('PC2', fontsize = 15)
#colors = ['r','b']
#target_names=[b'normal.', b'attack']
#for target_name, color in zip(target_names, colors):
#    indicesToKeep = end_df.iloc[:,2] == target_name
#    ax.scatter(end_df.loc[indicesToKeep, 'component 1'], end_df.loc[indicesToKeep, 'component 2'], c = color, s = 20)
#
#plt.show()

pca_scaler = StandardScaler().fit(prin_df.values)
scaled_pca_df = pca_scaler.transform(prin_df.values)

lb = LabelEncoder()
target_df = lb.fit_transform(target_df)

#split into training and testing
#use 20% for testing, seed rng, and make sure to shuffle, as input data is sorted.
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(scaled_pca_df, target_df, test_size = 0.2, random_state=57 )

##warning, SVM should have scaled data.
from sklearn import svm
clf = svm.SVC(verbose=True, max_iter=10000)
clf.fit(x_train, y_train)

y_pred = clf.predict(x_test)

#get confusion matrix

from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print("confusion matrix is" + str(cm))

from sklearn.model_selection import cross_val_score
acc = cross_val_score(estimator = clf, X = x_train, y = y_train, cv=10)
print("Mean Accuracy is " + str(acc.mean()) +  " with standard deviation " + str(acc.std()))
