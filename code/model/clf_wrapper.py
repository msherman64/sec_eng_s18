#!/usr/bin/env python3
import argparse

#define command line options
parser = argparse.ArgumentParser(description='Determine whether samples are normal or abnormal network traffic')
parser.add_argument('--capture_file', help="specify path to source .sample file to predict on")
parser.add_argument('--model_file', help="specify path to joblib saved classifer, either for save or load")
parser.add_argument('--clf', help="specify classifier: current options: oneclass, svc, isoforest, dummy")
parser.add_argument('--kernel', default='rbf', help="specify kernel for SVM methods")
parser.add_argument('--save', action="store_true", help="save classifer to path in --model_file")
parser.add_argument('--load', action="store_true", help="load classifier from path in --model_file instead of training")
parser.add_argument('--test', action="store_true", help="output confusion matrix for test set")
#parser.add_argument('--plot', action="store_true")
parser.add_argument('--validate', action="store_true", help="run 10 fold cross validation")
parser.add_argument('--optimize', action="store_true", help="compute optimal gamma")
parser.add_argument('--pca', action="store_true", help="run PCA to 2d before processing")
args = parser.parse_args()
print(args)

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

#load list of field names, stripped attack types ,includes whether it's a continuous or symbolic value
feature_list = pd.read_csv('source_files/kddcup.names',delimiter=':|\n',header=None,engine='python')
names = feature_list[0].tolist()

#Get dataset
from sklearn.datasets import fetch_kddcup99
#percent10=true dl 10% for testing, SA is mostly 'normal' values
dataset_10p = fetch_kddcup99(subset='SA',data_home='../../datasets/', percent10=False, shuffle=False, random_state=57)
#load data into dataframe, assign headers
dataframe = pd.DataFrame(dataset_10p.data, columns=names)
#replace python b_strings with regular strings
str_df = dataframe[['protocol_type','service','flag']]
str_df = str_df.stack().str.decode('utf-8').unstack()
for col in str_df:
    dataframe[col] = str_df[col]

#remove fields from both dataframe and names
extractor_names = pd.read_csv('source_files/extractor_output_header.txt').columns.tolist()
#if a field is not in extractor names, remove it from dataframe and names, excluding optional vals, as they're not in the dataset
dataframe = dataframe[extractor_names[:-6]]
feature_list = feature_list[feature_list[0].isin(extractor_names)]

#get which cols are symbolic or continuous from names file
sym_cols = feature_list.loc[feature_list[1].str.strip() == 'symbolic.'][0]
cont_cols = feature_list.loc[feature_list[1].str.strip() == 'continuous.'][0]


#do scaling normalize columns to 0-1. This prevents unfair weighting
#fit sets scaler params #transform applies, so we do same tranform to training and capture data
from sklearn.preprocessing import StandardScaler
# select features from training set, and fit normalizer
scaled_train_df = dataframe.copy()
cont_train_features = scaled_train_df[cont_cols]
scaler = StandardScaler().fit(cont_train_features.values)
#normalize training features, assign back to dataframe
scaled_train_features = scaler.transform(cont_train_features.values)
scaled_train_df[cont_cols] = scaled_train_features

#convert symbolic features via onehot encoder prior to normalizing. this ensures no effect of ordering.
#better way combines factorize and onehot encoding #fastml.com/how-to-use-pd-dot-get-dummies-with-the-test-set/
#do binary encoding of labels
#print("test set shape pre-onehot: " + str(scaled_train_df.shape))
scaled_onehot_train = pd.get_dummies(scaled_train_df, columns = sym_cols)
#print("test set shape: " + str(scaled_onehot_train.shape))

#process targets for classification, Go from Multi-Class to binary classification
target_df = pd.Series(dataset_10p.target, index=dataframe.index)
#fix b_string issue
target_df = target_df.str.decode('utf-8')
target_df = target_df.replace(
        ['back.', 'buffer_overflow.', 'ftp_write.', 'guess_passwd.', 'imap.', 'ipsweep.', 
            'land.', 'loadmodule.', 'multihop.', 'neptune.', 'nmap.', 'perl.', 'phf.', 
            'pod.', 'portsweep.', 'rootkit.', 'satan.', 'smurf.', 'spy.', 'teardrop.', 'warezclient.', 'warezmaster.'
            ], 'attack')
outliers = target_df[target_df == 'attack']
#print("outliers.shape", outliers.shape)
#set expected proportion of outliers
nu = outliers.shape[0]/target_df.shape[0]
#print("fraction of outliers " + str(nu))

#encode attacks to -1, normal to +1, replaces labelencoder. 
target_df.loc[target_df == 'normal.'] = 1
target_df.loc[target_df == 'attack'] = -1



if args.pca:
    from sklearn.decomposition import PCA
    pca_all = PCA(0.95)
    tmp = pca_all.fit_transform(scaled_onehot_train)
    print("Components needed for 95% variance: " + str(pca_all.n_components_))
    #we want to project to 2d for visualization
    pca = PCA(n_components=2)
    prinComp = pca.fit_transform(scaled_onehot_train)
    print("Variance explained per component: " + str(pca.explained_variance_ratio_))
    scaled_onehot_train = pd.DataFrame(data=prinComp, columns = ['component 1', 'component 2'])

#split into training and testing
#use 20% for testing, seed rng, and make sure to shuffle, as input data is sorted.
from sklearn.model_selection import train_test_split
#ensure shuffled, and keep state
x_train, x_test, y_train, y_test = train_test_split(scaled_onehot_train, target_df, test_size = 0.2, random_state=57, shuffle=True)
print("created training set, size " + str(x_train.shape))


###warning, SVM should have scaled data.
from sklearn import svm
from sklearn import dummy
from sklearn.externals import joblib
from sklearn import metrics
if args.load:
    clf = joblib.load(args.model_file)
else:
    if args.clf == 'oneclass':
        clf = svm.OneClassSVM(nu=nu, kernel=args.kernel,gamma=0.1, verbose=True)
        clf.fit(x_train)
    if args.clf == 'svc':
        clf = svm.SVC(verbose=True, max_iter=-1, class_weight='balanced')
        clf.fit(x_train, y_train)
    if args.clf == 'isoforest':
        from sklearn import ensemble
        clf = ensemble.IsolationForest(contamination=nu, n_jobs=-1, verbose=True)
        clf.fit(x_train)
    if args.clf == 'dummy':
        clf = dummy.DummyClassifier()
        clf.fit(x_train, y_train)
    print("fit model, starting prediction")
if args.save:
    #save model to disk
    joblib.dump(clf, args.model_file)

if args.test:
    y_pred = clf.predict(x_test)
    #get confusion matrix
    cm = metrics.confusion_matrix(y_test, y_pred)
    print("confusion matrix for testing is: " + str(cm))
    print("accuracy: ", metrics.accuracy_score(y_test, y_pred))
    print("precision: ", metrics.precision_score(y_test, y_pred))
    print("recall: ", metrics.recall_score(y_test, y_pred))
    print("f1: ", metrics.f1_score(y_test, y_pred))
    print("auc: ", metrics.roc_auc_score(y_test, y_pred))
    print("aps: ", metrics.average_precision_score(y_test, y_pred))

    print(metrics.classification_report(y_test, y_pred, target_names=['attack','normal']))

    y_score = clf.decision_function(x_test)
    print("auc weighted: ", metrics.roc_auc_score(y_test, y_score))
    print("aps weighted: ", metrics.average_precision_score(y_test, y_score))

#    from sklearn.metrics import precision_recall_curve, roc_curve
#    precision, recall, thresh = precision_recall_curve(y_test, y_score)
#    fpr, tpr, rocthresh = roc_curve(y_test, y_score)
#    print(thresh)
#    plt.step( recall, precision, color='b', alpha=0.2, where='post')
#    plt.step( fpr, tpr, color='r', alpha=0.2, where='post')
#    plt.fill_between(recall, precision, step='post', alpha=0.2, color='b')
#    plt.xlabel('Recall')
#    plt.ylabel('Precision')
#    plt.ylim([0.0, 1.05])
#    plt.xlim([0.0, 1.05])
#    plt.show()
    from scipy import stats
    threshold = stats.scoreatpercentile(y_score, 100 * nu)
    xx, yy = np.meshgrid(np.linspace(-10, 20, 500), np.linspace(-2, 15, 500))
    Z = clf.decision_function(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    print("Z Shape is: " + str(Z.shape))
    plt.title("Novelty")
    plt.contourf(xx, yy, Z, levels=np.linspace(threshold, Z.max(), 7), cmap=plt.cm.Blues_r)
    a = plt.contour(xx, yy, Z, levels=[threshold], linewidths=2, colors='red')
    plt.contourf(xx, yy, Z, levels=[Z.min(), threshold], colors='palevioletred')
    s = 20
    x_attack = x_train[y_train == -1]
    x_norm = x_train[y_train == 1]
    xt_attack = x_test[y_test == -1]
    xt_norm = x_test[y_test == 1]
    b1 = plt.scatter(x_attack.iloc[:, 0], x_attack.iloc[:, 1], c='white', s=s, edgecolors='k')
    b2 = plt.scatter(x_norm.iloc[:, 0], x_norm.iloc[:, 1], c='blueviolet', s=s, edgecolors='k')
    b3 = plt.scatter(xt_attack.iloc[:, 0], xt_attack.iloc[:, 1], c='black', s=s, edgecolors='k')
    b4 = plt.scatter(xt_norm.iloc[:, 0], xt_norm.iloc[:, 1], c='blue', s=s, edgecolors='k')

    plt.legend(
            [a.collections[0], b1, b2, b3, b4],
            ['decision function', 'attacks in training set', 'normal in training set', 
                'attacks in test set', 'normal in test set'],
            loc='lower right')

    plt.show()

if args.validate:
    from sklearn.model_selection import cross_val_score, cross_validate
    #cross validation needs "test" method
    scorer_acc = metrics.make_scorer(metrics.accuracy_score)
    scoring_list = {'accuracy', 'precision', 'recall', 'roc_auc', 'average_precision'}
#    scoring_list = {'accuracy': metrics.make_scorer(metrics.accuracy_score)}
    #scores = cross_val_score(estimator = clf, X = x_train, y = y_train, cv=10, n_jobs=-1, scoring=scoring_list)
    scores = cross_validate(estimator = clf, X = x_train, y = y_train, cv=10, n_jobs=-1, scoring=scoring_list)
#    print("Mean Accuracy is " + str(acc.mean()) +  " with standard deviation " + str(acc.std()))
    print(scores.keys())
    print(scores['test_roc_auc'].mean())
    print(scores['test_average_precision'].mean())
if args.optimize:
    from sklearn.model_selection import GridSearchCV
    scores = {'precision', 'recall'}
    parameters = [{'kernel' : ['rbf'], 'gamma': [1e-1, 1e-4]}]
    opt = GridSearchCV(svm.OneClassSVM(), parameters, cv=5, scoring=scores, n_jobs=-1, refit='precision')
    opt.fit(x_train, y_train)
    print("Best Params found: " + str(opt.best_params_))


# Run on captured data
#this sequence should be turned into a sckit pipeline
#import captured data
if args.capture_file:
    cap_names = feature_list[0].tolist()
    cap_names.append('trace_type')
    cap_df = pd.read_csv(args.capture_file, names=cap_names, header=None)

    cap_target = cap_df['trace_type']
    #encode attacks to -1, normal to +1, replaces labelencoder. 
    normal_loc = cap_target == 'normal'
    attack_loc = cap_target == 'attack'
    if not normal_loc.empty:
        cap_target.loc[normal_loc] = 1
    if not attack_loc.empty:
        cap_target.loc[attack_loc] = -1
    #cap_target.loc[cap_target == 'normal'] = 1
    #cap_target.loc[cap_target == 'attack'] = -1
    cap_df.drop(columns=['trace_type'])

    #normalize captured features, assign back to df
    scaled_cap_df = cap_df.copy()
    cont_cap_features = scaled_cap_df[cont_cols]
    scaled_cap_features = scaler.transform(cont_cap_features.values)
    scaled_cap_df[cont_cols] = scaled_cap_features
    #encode cap data
    scaled_onehot_cap = pd.get_dummies(scaled_cap_df, columns = sym_cols)
    
    #need to add missing columns to captured set
    missing_columns = set(scaled_onehot_train.columns) - set(scaled_onehot_cap.columns)
    for col in missing_columns:
        scaled_onehot_cap[col] = 0
    #remove extra columns not present in trainng set
    scaled_onehot_cap = scaled_onehot_cap[scaled_onehot_train.columns]
   
    #output estimate of training data
    cap_pred = clf.predict(scaled_onehot_cap)
    unique, counts = np.unique(cap_pred, return_counts=True)
    print(unique)
    print(counts)
    #output results
    #print(cap_pred)
    #print(cap_target)
    print(metrics.classification_report(cap_target, cap_pred, target_names=['attack','normal']))

def preprocess_data(arg_df):
    cont_cap_features = arg_df[cont_cols]
    scaled_cap_features = scaler.transform(cont_cap_features.values)
    arg_df[cont_cols] = scaled_cap_features
    #encode cap data
    arg_df = pd.get_dummies(arg_df, columns = sym_cols)

