#! /usr/bin/python3
from sklearn.datasets import fetch_kddcup99

#available datasets none=entire, SA, SF, http, smtp
#data_home = fast cache?
#percent10=true dl 10% for testing

dataset_10p = fetch_kddcup99(subset='SA',data_home='../../datasets/', percent10='true')

#print summary
from pprint import pprint
pprint(dataset_10p.data)
pprint(dataset_10p.target)

#need to pre-process, turn strings into numeric identifiers of some kind
    # http://scikit-learn.org/stable/datasets/index.html#general-dataset-api
    # http://scikit-learn.org/stable/modules/preprocessing.html#encoding-categorical-features

import pandas as pd
enc_data = dataset_10p.data
enc_target = dataset_10p.target
symbolic_cols = [1,2,3]
for i in symbolic_cols:
    enc_data[:,i], unique = pd.factorize(enc_data[:,i])

enc_target, unique = pd.factorize(enc_target)
pprint(enc_data)
pprint(enc_target)

#normalize data for svm
from sklearn import preprocessing

min_max_scaler = preprocessing.MinMaxScaler()
scaler_fit = min_max_scaler.fit(enc_data)
enc_data_scaled = scaler_fit.transform(enc_data)
#enc_target_scaled = scaler_fit.transform(enc_target)
#important, must ensure data and target scaled the same!!!!

pprint(enc_data_scaled)


pprint(enc_data_scaled[5480])
#pprint(enc_target_scaled[5480])


#
##try svm first? novelty detection
#naieve svm way too slow, try linear
#

#warning, SVM should have scaled data.
from sklearn import svm
#clf = svm.LinearSVC(verbose=True, max_iter=1000)
clf = svm.SVC(verbose=True, max_iter=1000)
clf.fit(enc_data_scaled, enc_target)
#
#
