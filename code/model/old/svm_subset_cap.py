#!/usr/bin/env python3

import pandas as pd

#load list of field names, stripped attack types ,includes whether it's a continuous or symbolic value
feature_list = pd.read_csv('../../datasets/pandas/kddcup.names',delimiter=':|\n',header=None,engine='python')
names = feature_list[0].tolist()

from sklearn.datasets import fetch_kddcup99
#percent10=true dl 10% for testing, SA is mostly 'normal' values
dataset_10p = fetch_kddcup99(subset='SA',data_home='../../datasets/', percent10='true', shuffle=True)
#load data into dataframe, assign headers
dataframe = pd.DataFrame(dataset_10p.data, columns=names)
#replace python b_strings with regular strings
str_df = dataframe[['protocol_type','service','flag']]
str_df = str_df.stack().str.decode('utf-8').unstack()
for col in str_df:
    dataframe[col] = str_df[col]

#remove fields from both dataframe and names
extractor_names = pd.read_csv('extractor_output_header.txt').columns.tolist()
#if a field is not in extractor names, remove it from dataframe and names, excluding optional vals, as they're not in the dataset
dataframe = dataframe[extractor_names[:-6]]
feature_list = feature_list[feature_list[0].isin(extractor_names)]

#get which cols are symbolic or continuous from names file
sym_cols = feature_list.loc[feature_list[1].str.strip() == 'symbolic.'][0]
cont_cols = feature_list.loc[feature_list[1].str.strip() == 'continuous.'][0]

#import captured data
import sys

cap_df = pd.read_csv(sys.argv[1], names=feature_list[0].tolist())

#do scaling normalize columns to 0-1. This prevents unfair weighting
#fit sets scaler params #transform applies, so we do same tranform to training and capture data
from sklearn.preprocessing import StandardScaler,MultiLabelBinarizer,LabelBinarizer,LabelEncoder
# select features from training set, and fit normalizer
scaled_train_df = dataframe.copy()
cont_train_features = scaled_train_df[cont_cols]
scaler = StandardScaler().fit(cont_train_features.values)
#normalize training features, assign back to dataframe
scaled_train_features = scaler.transform(cont_train_features.values)
scaled_train_df[cont_cols] = scaled_train_features
#normalize captured features, assign back to df
scaled_cap_df = cap_df.copy()
cont_cap_features = scaled_cap_df[cont_cols]
scaled_cap_features = scaler.transform(cont_cap_features.values)
scaled_cap_df[cont_cols] = scaled_cap_features

#convert symbolic features via onehot encoder prior to normalizing. this ensures no effect of ordering.
#better way combines factorize and onehot encoding #fastml.com/how-to-use-pd-dot-get-dummies-with-the-test-set/

#do binary encoding of labels
scaled_onehot_train = pd.get_dummies(scaled_train_df, columns = sym_cols)
scaled_onehot_cap = pd.get_dummies(scaled_cap_df, columns = sym_cols)

#need to add missing columns to captured set
missing_columns = set(scaled_onehot_train.columns) - set(scaled_onehot_cap)
for col in missing_columns:
    scaled_onehot_cap[col] = 0
#remove extra columns not present in trainng set
scaled_onehot_cap = scaled_onehot_cap[scaled_onehot_train.columns]

#process targets for classification, Go from Multi-Class to binary classification
target_df = pd.Series(dataset_10p.target, index=dataframe.index)
#fix b_string issue
target_df = target_df.str.decode('utf-8')
target_df = target_df.replace(
        ['back.', 'buffer_overflow.', 'ftp_write.', 'guess_passwd.', 'imap.', 'ipsweep.', 
            'land.', 'loadmodule.', 'multihop.', 'neptune.', 'nmap.', 'perl.', 'phf.', 
            'pod.', 'portsweep.', 'rootkit.', 'satan.', 'smurf.', 'spy.', 'teardrop.', 'warezclient.', 'warezmaster.'
            ], 'attack')
lb = LabelEncoder()
lb.fit(target_df)
target_df = lb.transform(target_df)

#split into training and testing
#use 20% for testing, seed rng, and make sure to shuffle, as input data is sorted.
from sklearn.model_selection import train_test_split
x_train, x_test, y_train, y_test = train_test_split(scaled_onehot_train, target_df, test_size = 0.2, random_state=57 )

##warning, SVM should have scaled data.
from sklearn import svm
clf = svm.SVC(verbose=False, max_iter=1000)
clf.fit(x_train, y_train)

y_pred = clf.predict(x_test)

#get confusion matrix
from sklearn.metrics import confusion_matrix
cm = confusion_matrix(y_test, y_pred)
print("confusion matrix for testing is: " + str(cm))

#from sklearn.model_selection import cross_val_score
#acc = cross_val_score(estimator = clf, X = x_train, y = y_train, cv=10)
#print("Mean Accuracy is " + str(acc.mean()) +  " with standard deviation " + str(acc.std()))

#output estimate of training data
import numpy as np
cap_pred = clf.predict(scaled_onehot_cap)
inverse_pred = lb.inverse_transform(cap_pred)
unique, counts = np.unique(inverse_pred, return_counts=True)
print(unique)
print(counts)
