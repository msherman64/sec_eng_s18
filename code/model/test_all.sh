#!/bin/bash
model_dir='100p_models'
for trace in ../collection/traces/*.sample; do
    tracename=`basename $trace`
    echo $tracename
    ./clf_wrapper.py --load --model_file $model_dir/isoforest.pkl --capture_file $trace > output/isoforest_$tracename
    ./clf_wrapper.py --load --model_file $model_dir/oneclass.pkl --capture_file $trace > output/oneclass_$tracename
    ./clf_wrapper.py --load --model_file $model_dir/svc.pkl --capture_file $trace > output/svc_$tracename
    ./clf_wrapper.py --load --model_file $model_dir/dummy.pkl --capture_file $trace > output/dummy_$tracename
done
